$( document ).ready(function() {

    $('#reversi').on('click'), '.reversi.cell', function (event) {
        $this.animate(
            )
    }

    $('.upload').on('click', '.upload_label_file', function (event) {

        var $this = $(this);
        var $upload = $this.closest('.upload');

        console.log('clicked');

        if ( $upload.find('.upload_name').val() == '' ) {
            console.log('empty');
            $upload.find('.upload_name').addClass('error');
            $upload.find('.change_in_master').addClass('error');
            event.preventDefault()
        } else {
            return;
        }

    });

    $('.upload').on('change', '.upload_file', function() {
    console.log("Up");

        var $this = $(this);
        var $upload = $this.closest('.upload');

        // Check if name is given
        if ( $upload.find('.upload_name').val() == '' ) {
            console.log('empty');
            $upload.find('.upload_name').addClass('error');
            $upload.find('.upload_label_name').addClass('error');
            event.preventDefault()
            return;
        }

        // Post-Daten vorbereiten
        var data = new FormData();
        data.append('upload', this.files[0]);
        data.append('name', $upload.find('.upload_name').val());
        data.append('mode', $upload.find('.upload_mode').val());

        console.log("Name: " + $upload.find('.upload_name').val());

        // Ajax-Call
        $.ajax({
            url: "uploader.php",
            data: data,
            type: "POST",
            processData: false,
            contentType: false,
            success: function(evt) {
                console.log('Event: ' + evt);
                $upload.find('.upload_file').val('');
                $upload.find('.upload_msg').append($('<li>'+evt+'</li>'));
             }
        });

    });

    $('.download').each( function() {
        console.log('Download');
        var $this = $(this);
        var folder = $this.attr('folder');
        console.log(folder);
        $.ajax({
            url: folder,
            type: "GET",
            success: function (evt) {
                console.log(evt);
                evt = evt.replace(/<h1.*?<\/h1>/gi, '');
                evt = evt.replace(/<li.*?\/.*?<\/a>\s*<\/li>/gi, '');
                $download = $(evt);
                $download.find('a').each(function() {
                    $(this).attr('href', folder + '/' + $(this).attr('href'));
                })
                $this.append($download);
            }
        });
    });

});
